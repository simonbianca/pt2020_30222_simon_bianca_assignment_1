package Calculator;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import Controller.Controller;
import Model.Monomial;
import Model.Polynomial;
import View.View;

public class Calculator {
	
	public static void main(String[] args) {
	
		View view=new View();
		Polynomial model=new Polynomial();
		Controller controller=new Controller(view,model);
		view.setVisible(true);
	}

}
