package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JButton;

import Model.Monomial;
import Model.Polynomial;
import View.View;

public class Controller {

	private View view;
	private Polynomial model;

	public Controller(View view,Polynomial model) {
		this.view=view;
		this.model=model;
		this.view.addButtonIntegrationListener(new ButtonListener());
		this.view.addButtonDerivationListener(new ButtonListener());
		this.view.addButtonMultiplicationListener(new ButtonListener());
		this.view.addButtonSubtractionListener(new ButtonListener());
		this.view.addButtonAdditionListener(new ButtonListener());
	}
	
	class ButtonListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent e) {
		var o=(JButton) e.getSource();
		var label=o.getText();
		
		try {
		Polynomial polynomial1=new Polynomial();
		polynomial1=getFirstPolynomial();
		Polynomial polynomial2=new Polynomial();
		polynomial2=getSecondPolynomial();
		List<Monomial> result=new ArrayList<Monomial>();
		
		if(label.equalsIgnoreCase("+")) {
			result=polynomial1.addition(polynomial2);
			Collections.sort(result);
			Collections.reverse(result);
			Polynomial finalResult=new Polynomial(result);
			view.setResult(finalResult.toString());

		}
		else if(label.equalsIgnoreCase("-")) {
			result=polynomial1.subtraction(polynomial2);
			Collections.sort(result);
			Collections.reverse(result);
			Polynomial finalResult=new Polynomial(result);
			view.setResult(finalResult.toString());
			
		}
		else if(label.equalsIgnoreCase("*")) {
			result=polynomial1.multiplication(polynomial2);
			Collections.sort(result);
			Collections.reverse(result);
			Polynomial finalResult=new Polynomial(result);
			view.setResult(finalResult.toString());
		}
		else if(label.equalsIgnoreCase("~")) {
			result=polynomial1.integration();
			Collections.sort(result);
			Collections.reverse(result);
			Polynomial finalResult=new Polynomial(result);
			view.setResult(finalResult.toStringIntegration());
		}
		else if(label.equalsIgnoreCase("`")) {
			result=polynomial1.derivation();
			Collections.sort(result);
			Collections.reverse(result);
			Polynomial finalResult=new Polynomial(result);
			view.setResult(finalResult.toString());
		}
		}
		catch(NumberFormatException exc) {
			view.displayErrorMessage("The polynomial is not valid.\nPlease enter another polynomial!");
			view.resetResult();
			}
		catch(NoSuchElementException exc) {
			view.displayErrorMessage("The polynomial is not valid.\nPlease enter another polynomial!");
			view.resetResult();
			}
		catch(NullPointerException exc) {
			view.displayErrorMessage("The polynomial is not valid.\nPlease enter another polynomial!");
			view.resetResult();
			}
		}
		
		public Polynomial getFirstPolynomial() {	
		model=view.getPolynomial(view.getStringOfPolynomial1());
			return model;
		}
		public Polynomial getSecondPolynomial() {
			model=view.getPolynomial(view.getStringOfPolynomial2());
			return model;
		}
	}
	
}
