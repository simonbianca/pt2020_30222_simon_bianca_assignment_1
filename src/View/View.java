package View;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.Monomial;
import Model.Polynomial;

public class View extends JFrame{
	
	private String pattern1="((\\+|-)?([0-9])*x\\^([0-9])*)*(\\+|-)?([0-9])*";
	private JTextField firstPolynomial=new JTextField(25);
	private JTextField secondPolynomial=new JTextField(25);
	private JTextField result=new JTextField(34);
	private JTextField polynomial1=new JTextField(8);
	private JTextField polynomial2=new JTextField(8);
	private JButton buttonAddition=new JButton("+");
	private JButton buttonSubtraction=new JButton("-");
	private JButton buttonMultiplication=new JButton("*");
	private JButton buttonDerivation=new JButton("`");
	private JButton buttonIntegration=new JButton("~");
	
	public View() {
		init();
	}
	
	private void init() {
		setTitle("Polynomial calculator");
		setSize(400,180);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JPanel panel=new JPanel();
		panel.add(polynomial1);
		panel.add(firstPolynomial);
		panel.add(polynomial2);
		panel.add(secondPolynomial);
		panel.add(result);
		panel.add(Box.createVerticalStrut(40));
		panel.add(buttonAddition);
		panel.add(buttonSubtraction);
		panel.add(buttonMultiplication);
		panel.add(buttonIntegration);
		panel.add(buttonDerivation);

		result.setEditable(false);
		polynomial1.setEditable(false);
		polynomial2.setEditable(false);
		polynomial1.setText("1st polynomial");
		polynomial2.setText("2nd polynomial");
		
		buttonIntegration.setPreferredSize(new Dimension(60,30));
		buttonDerivation.setPreferredSize(new Dimension(60,30));
		buttonMultiplication.setPreferredSize(new Dimension(60,30));
		buttonAddition.setPreferredSize(new Dimension(60,30));
		buttonSubtraction.setPreferredSize(new Dimension(60,30));
	
		this.add(panel);
	}

	public String getStringOfPolynomial1() {
		return firstPolynomial.getText().toString();
	}
	
	public String getStringOfPolynomial2() {
		return secondPolynomial.getText().toString();
	}
	// pentru cazurile in care s-a introdus semnul + sau -, sau o cifra
	public String constructStringOfPolynomial(String string) {
		if(validationPolynomial(string)) {
			if(string.equalsIgnoreCase("+") || string.equalsIgnoreCase("-") || string.length()==0) {
				string="0";
			}
			else if(string.matches("(\\+|-)?([0-9])*")) {
				string=string+"x^0";
			}
			int index1=string.lastIndexOf("+");
			if(index1>=0) {
				if(string.substring(index1).matches("(\\+|-)?([0-9])*") && string.substring(index1).length()>=2) {
				string=string+"x^0";
				}
			}
			int index2=string.lastIndexOf("-");
			if(index2>=0) {
				if(string.substring(index2).matches("(\\+|-)?([0-9])*")&& string.substring(index2).length()>=2) {
				string=string+"x^0";
				}
			}
			return string;
		}
		return null;
	}
	//imparte String-ul
	public List<Integer> splitString(String string){
		String[] array1=string.split("\\^");
		List<Integer> list=new ArrayList<Integer>();
		for(String string1:array1) {
			//pentru cazurile in care nu s-a adaugat si cifra 1
			if(string1.startsWith("+x")) {
				string1="+1x";
			}
			else if(string1.startsWith("-x")) {
				string1="-1x";
			}
			else if(string1.startsWith("x")) {
				string1="1x";
			}
			String[] array2=string1.split("x");
			for(String string2:array2) {
				if(string2.endsWith("+") || string2.endsWith("-")) {
					string2=string2+"1";
				}
				if(string2.startsWith("+")) {
					string2=string2.substring(1);
				}
				String[] array3=string2.split("[+]");
				for(String string3:array3) {
					if(string3.startsWith("-")) {
						list.add(Integer.parseInt(string3));
					}
					else if(string3.contains("-")) {
						String[] array4=string3.split("[-]");
						String firstElement=array4[0];
						String secondElement=array4[1];
						list.add(Integer.parseInt(firstElement));
						list.add(Integer.parseInt(secondElement)*(-1));
					}
					else{
						list.add(Integer.parseInt(string3));
					}
				}
			}
		}
		return list;
	}
	
	public Polynomial getPolynomial (String str) {
		String string=constructStringOfPolynomial(str);
		List<Monomial> listOfMonomials=new ArrayList<Monomial>();
		List<Integer> list=new ArrayList<Integer>();
		list=splitString(string);
		ListIterator<Integer> itr=list.listIterator();
		while(itr.hasNext()) {
			int coefficient=(int) itr.next();
			int rank=(int)itr.next();
			listOfMonomials.add(new Monomial(coefficient,rank));
		}
		Polynomial polynomial=new Polynomial(listOfMonomials);
		Collections.sort(polynomial.getPolynomial());
		Collections.reverse(polynomial.getPolynomial());
		polynomial=polynomial.reorganisationPol();
		return polynomial;
	}
	

	public void setResult(String str) {
		result.setText(str);
	}
	public void resetResult() {
		result.setText("");
	}
	
	public void addButtonIntegrationListener(ActionListener listenForButtonIntegration) {
		buttonIntegration.addActionListener(listenForButtonIntegration);
	}
	public void addButtonDerivationListener(ActionListener listenForButtonDerivation) {
		buttonDerivation.addActionListener(listenForButtonDerivation);
	}
	
	public void addButtonMultiplicationListener(ActionListener listenForButtonMultiplication) {
		buttonMultiplication.addActionListener(listenForButtonMultiplication);
	}
	public void addButtonSubtractionListener(ActionListener listenForButtonSubtraction) {
		buttonSubtraction.addActionListener(listenForButtonSubtraction);
	}
	
	public void addButtonAdditionListener(ActionListener listenForButtonAddition) {
		buttonAddition.addActionListener(listenForButtonAddition);
	}
	public void displayErrorMessage(String erorMessage) {
		JOptionPane.showMessageDialog(this, erorMessage);
	}
	
	public boolean validationPolynomial(String string) {
		if(string.matches(pattern1)){
			return true;
		}
		return false;
	}
	
}
