package Model;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Polynomial  {
	
	final DecimalFormat df2= new DecimalFormat("#.##");
	private List <Monomial> polynomial=new ArrayList<Monomial>();
	
	public Polynomial() {
		this.polynomial=new ArrayList<Monomial>();
	}
	public Polynomial(List<Monomial> polynomial) {
		this.polynomial = polynomial;
	}
	
	public List<Monomial> getPolynomial() {
		return polynomial;
	}

	public void setPolynomial(List<Monomial> polynomial) {
		this.polynomial = polynomial;
	}

	//Operatia de adunare
	
	public List<Monomial> addition(Polynomial secondPolynomial){	
		List<Monomial> result=new ArrayList<Monomial>();
		for(Monomial item1: this.polynomial){
			for(Monomial item2: secondPolynomial.getPolynomial()){
				if(item1.getRank()==item2.getRank()){
					if(validationAdd(item2.getRank(),result)) {
						result.add(new Monomial(item1.getCoefficient()+item2.getCoefficient(),item1.getRank()));
					}
				}
				else if(item1.getRank()>item2.getRank()){	
					//verific daca s-a adaugat deja monomul de grad x
					if(validationAdd(item1.getRank(),result)) {
						result.add(new Monomial(item1.getCoefficient(),item1.getRank()));
					}
				}
				else if(item1.getRank()<item2.getRank()){
					if(validationAdd(item2.getRank(),result)) {
						result.add(new Monomial(item2.getCoefficient(),item2.getRank()));
					}
				}
					
			}
		}
		//adaug monoamele ramase, avand grad mai mic fata de ultimul monom din rezultat
				for(Monomial item1:this.polynomial) {
					if(item1.getRank()<result.get(result.size()-1).getRank()) {
						result.add(new Monomial(item1.getCoefficient(),item1.getRank()));
					}
				}
				//adaug monoamele ramase
				for(Monomial item2:secondPolynomial.getPolynomial()) {
					if(item2.getRank()<result.get(result.size()-1).getRank()) {
						result.add(new Monomial(item2.getCoefficient(),item2.getRank()));
					}
				}
		return result;
	}
	public boolean validationAdd(int rank,List<Monomial> result) {
		for(Monomial itemr: result) {
			if(itemr.getRank()==rank) {
				return false;
			}
		}
		return true;
	}
	
	//Operatia de scadere
	
	public List<Monomial> subtraction(Polynomial secondPolynomial){
		List<Monomial> result=new ArrayList<Monomial>();
		for(Monomial item1: this.polynomial){
			for(Monomial item2: secondPolynomial.getPolynomial()){
				if(item1.getRank()==item2.getRank()){
					if(validationAdd(item2.getRank(),result)) {
						result.add(new Monomial(item1.getCoefficient()-item2.getCoefficient(),item1.getRank()));
					}
				}
				else if(item1.getRank()>item2.getRank()){	
					//verific daca s-a adaugat deja monomul de grad x
					if(validationAdd(item1.getRank(),result)){
						result.add(new Monomial(item1.getCoefficient(),item1.getRank()));
					}
				}
				else if(item1.getRank()<item2.getRank()){
					if(validationAdd(item2.getRank(),result)){
						result.add(new Monomial(-(item2.getCoefficient()),item2.getRank()));
					}
				}	
			}
		}
		//adaug monoamele ramase, avand grad mai mic fata de ultimul monom din rezultat
				for(Monomial item1:this.polynomial) {
					if(item1.getRank()<result.get(result.size()-1).getRank()) {
						result.add(new Monomial(item1.getCoefficient(),item1.getRank()));
					}
				}
				//adaug monoamele ramase, inmultindu-le cu (-1)
				for(Monomial item2:secondPolynomial.getPolynomial()) {
					if(item2.getRank()<result.get(result.size()-1).getRank()) {
						result.add(new Monomial(-(item2.getCoefficient()),item2.getRank()));
					}
				}
		return result;
	}
	
	//Operatia de inmultire
	public List<Monomial> multiplication(Polynomial secondPolynomial){
		List <Monomial> result=new ArrayList<Monomial>();
		// am inmultit cele doua polinoame
		for(Monomial item1:this.polynomial) {
			for(Monomial item2:secondPolynomial.getPolynomial()) {
				result.add(new Monomial(item1.getCoefficient()*item2.getCoefficient(),item1.getRank()+item2.getRank()));
			}
		}
		// am sortat polinomul rezultat in ordine crescatoare
		Collections.sort(result);
		Collections.reverse(result);
		List <Monomial> finalResult=new ArrayList<Monomial>();
		finalResult.add(new Monomial(0,result.get(0).getRank()));
		// am adunat monoamele care au acelasi rang
		for(Monomial item1:result) {
			if(item1.getRank()==finalResult.get(finalResult.size()-1).getRank()) {
				finalResult.get(finalResult.size()-1).setRank(item1.getRank());
				finalResult.get(finalResult.size()-1).setCoefficient(item1.getCoefficient()+finalResult.get(finalResult.size()-1).getCoefficient());
			}
			else {
				finalResult.add(new Monomial(item1.getCoefficient(),item1.getRank()));
			}
		}
		return finalResult;
	}
	
	// Operatia de derivare
	
	public List<Monomial> derivation(){
		List<Monomial> result=new ArrayList<Monomial>();
		for(Monomial item:this.polynomial) {
			if(item.getRank()>=1) {
				result.add(new Monomial(item.getRank()*item.getCoefficient(),item.getRank()-1));
			}
		}
		return result;
	}
	
	// Operatia de integrqare
	
	public List<Monomial> integration(){
		List<Monomial> result=new ArrayList<Monomial>();
		for(Monomial item:this.polynomial) {
			result.add(new Monomial(item.getCoefficient(),item.getRank()+1,item.getRank()+1));
		}
		return result;
	}
	@Override // pentru afisarea rezultatului
	public String toString() {
		String string="";
		List<String> list=new ArrayList<String>();
		for(Monomial item:this.polynomial) {
			if(item.getCoefficient()>0) {
				list.add("+"+item.getCoefficient()+"x^"+item.getRank());
			}
			else if(item.getCoefficient()!=0){
				list.add(item.getCoefficient()+"x^"+item.getRank());
			}
		}
		for(String item1:list) {
			string=string+""+item1;
		}
		return string;
	}
	//pentru afisarea rezultatului pentru integrare
	public String toStringIntegration() {
		String string="";
		List<String> list=new ArrayList<String>();
		for(Monomial item:this.polynomial) {
			if(item.getCoefficientIntegration()>0) {
				list.add("+"+(df2.format(item.getCoefficientIntegration()))+"x^"+item.getRank());
			}
			else if(item.getCoefficientIntegration()!=0){
				list.add(df2.format(item.getCoefficientIntegration())+"x^"+item.getRank());
			}
		}
		for(String item1:list) {
			string=string+""+item1;
		}
		return string;
	}
	//daca sunt introduse polinoame avand monoame de grad egal, sunt adunate rezultand un singur polinom
	public Polynomial reorganisationPol() {
		List <Monomial> finalResult=new ArrayList<Monomial>();
		finalResult.add(new Monomial(0,this.polynomial.get(0).getRank()));
		for(Monomial item1:this.polynomial) {
			if(item1.getRank()==finalResult.get(finalResult.size()-1).getRank()) {
				finalResult.get(finalResult.size()-1).setRank(item1.getRank());
				finalResult.get(finalResult.size()-1).setCoefficient(item1.getCoefficient()+finalResult.get(finalResult.size()-1).getCoefficient());
			}
			else {
				finalResult.add(new Monomial(item1.getCoefficient(),item1.getRank()));
			}
		}
		Polynomial pol=new Polynomial(finalResult);
		return pol;
	}

	
}
