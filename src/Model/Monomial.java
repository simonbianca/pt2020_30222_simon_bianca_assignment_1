package Model;

import java.util.ArrayList;
import java.util.List;

public class Monomial implements Comparable<Monomial>{
	
	private int coefficient;
	private int rank;
	private double coefficientIntegration;
	
	public Monomial(int coefficient, int rank) {
		this.coefficient = coefficient;
		this.rank = rank;
	}
	public Monomial(int coefficient,int coefficientDenominator,int rank) {
		Double coef=(double) (coefficient);
		Double coef2=(double) (coefficientDenominator);
		this.coefficientIntegration=coef/coef2;
		this.coefficient=1;
		this.rank=rank;
	}
	public int getCoefficient() {
		return coefficient;
	}
	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public double getCoefficientIntegration() {
		return coefficientIntegration;
	}
	public void setCoefficientIntegration(double coefficientIntegration) {
		this.coefficientIntegration = coefficientIntegration;
	}
	@Override
	public int compareTo(Monomial monomial) {
		Integer item=this.getRank();
		return item.compareTo(monomial.getRank());
	}
}